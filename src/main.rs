use differential_equations::{self, Method, ODEProblem};

fn lorenz(mut du: Vec<f64>, u: Vec<f64>, t: f64) -> Vec<f64> {
    du[0] = 10.0 * (u[1] - u[0]);
    du[1] = u[0] * (28.0 - u[2]) - u[1];
    du[2] = u[0] * u[1] - (8. / 3.) * u[2];
    du
}

fn main() {
    let problem = ODEProblem {
        system: lorenz, // As a function
        u0: None,
        method: None,
    };
    //system: |x| x * 2.,   // As a closure
    //u0: Some(vec![0., 0., 0.]),
    // method: Some(String::from("Runge kutta")),
    problem.solve();
    differential_equations::say_lib();
}
